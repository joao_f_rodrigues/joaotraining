﻿using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceBusTopic
{
    class Program
    {

        public static String connectionString = "Endpoint=sb://rmsdev.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=bwLNVj1o3Ex8vbd0QtiNbrPcT5JuwWeYYu40vAEDdAM=";
        public static String topicName = "sonictopic";
        public static TopicClient topicClient;

        public static string[] Subscriptions = { "CGI", "NotCGI"};
        static IDictionary<string, string[]> SubscriptionFilters = new Dictionary<string, string[]> {
            { "CGI", new[] { "companyID = 'CGI'" } },
            { "NotCGI", new[] { "companyID != 'CGI'" } }
        };
        // You can have only have one action per rule and this sample code supports only one action for the first filter which is used to create the first rule. 
        static IDictionary<string, string> SubscriptionAction = new Dictionary<string, string> {
            { "CGI", "" },
            { "NotCGI", "" },
        };
        static string[] Store = { "CGI", "NotCGI" };
        static string SysField = "sys.To";
        static string CustomField = "StoreId";
        static int NrOfMessagesPerStore = 1; // Send at least 1.


        private static Random random = new Random();
        static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
        }

        public static async Task MainAsync()
        {
            const int numberOfMessages = 10;
            topicClient = new TopicClient(connectionString, topicName);

            // Send messages.
            await RemoveDefaultFilters();
            await CleanUpCustomFilters();
            await CreateCustomFilters();
            await SendMessages();

            Console.ReadKey();

            await topicClient.CloseAsync();
        }


        public static async Task SendMessages()
        {
            try
            {
                

                var taskList = new List<Task>();
                for (int i = 0; i < 100; i++)
                {
                    taskList.Add(SendItems(topicClient, Store[random.Next(0,2)]));
                }

                await Task.WhenAll(taskList);
                await topicClient.CloseAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine("\nAll messages sent.\n");
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static long LongRandom(long min, long max, Random rand)
        {
            byte[] buf = new byte[8];
            rand.NextBytes(buf);
            long longRand = BitConverter.ToInt64(buf, 0);
            return (Math.Abs(longRand % (max - min)) + min);
        }

        private static async Task SendItems(TopicClient tc, string store)
        {
            for (int i = 0; i < NrOfMessagesPerStore; i++)
            {
                Login login = new Login();
                login.timestamp = DateTime.Now.Ticks;
                login.username = RandomString(random.Next(1, 9));
                login.email = RandomString(random.Next(1, 9));
                login.device = RandomString(random.Next(1, 9));
                login.lastLogin = LongRandom(100000000000000000, 600000000000000000, random);
                string JSONresult = JsonConvert.SerializeObject(login);

                // Note the extension class which is serializing an deserializing messages
                var message = new Message(Encoding.UTF8.GetBytes(JSONresult));
                message.To = store;
                message.UserProperties.Add("companyID", store);

                await tc.SendAsync(message);
                Console.WriteLine($"Sent Login from {store}. ;");
            }
        }

        private static async Task CleanUpCustomFilters()
        {
            foreach (var subscription in Subscriptions)
            {
                try
                {
                    SubscriptionClient s = new SubscriptionClient(connectionString, topicName, subscription);
                    IEnumerable<RuleDescription> rules = await s.GetRulesAsync();
                    foreach (RuleDescription r in rules)
                    {
                        await s.RemoveRuleAsync(r.Name);
                        Console.WriteLine($"Rule {r.Name} has been removed.");
                    }
                    await s.CloseAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERROR CREATING RULES, PROBABLY ALREADY EXISTS") ;
                }
            }
            Console.WriteLine("All default filters have been removed.\n");

        }

        private static async Task RemoveDefaultFilters()
        {
            Console.WriteLine($"Starting to remove default filters.");

            try
            {
                foreach (var subscription in Subscriptions)
                {
                    SubscriptionClient s = new SubscriptionClient(connectionString, topicName, subscription);
                    await s.RemoveRuleAsync(RuleDescription.DefaultRuleName);
                    Console.WriteLine($"Default filter for {subscription} has been removed.");
                    await s.CloseAsync();
                }

                Console.WriteLine("All default Rules have been removed.\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine("NO DEFAULT FILTERS");
            }

        }

        private static async Task CreateCustomFilters()
        {
            try
            {
                for (int i = 0; i < Subscriptions.Length; i++)
                {
                    SubscriptionClient s = new SubscriptionClient(connectionString, topicName, Subscriptions[i]);
                    string[] filters = SubscriptionFilters[Subscriptions[i]];
                    if (filters[0] != "")
                    {
                        int count = 0;
                        foreach (var myFilter in filters)
                        {
                            count++;

                            string action = SubscriptionAction[Subscriptions[i]];
                            if (action != "")
                            {
                                await s.AddRuleAsync(new RuleDescription
                                {
                                    Filter = new SqlFilter(myFilter),
                                    Action = new SqlRuleAction(action),
                                    Name = $"MyRule{count}"
                                });
                            }
                            else
                            {
                                await s.AddRuleAsync(new RuleDescription
                                {
                                    Filter = new SqlFilter(myFilter),
                                    Name = $"MyRule{count}"
                                });
                            }
                        }
                    }

                    Console.WriteLine($"Filters and actions for {Subscriptions[i]} have been created.");
                }

                Console.WriteLine("All filters and actions have been created.\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR CREATING FILTERS");
            }

        }
    }
}
