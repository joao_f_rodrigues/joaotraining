using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FunctionApp2
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static void Run([ServiceBusTrigger("sonicqueue", Connection = "connectionString")]string myQueueItem, ILogger log)
        {

            try
            {

                log.LogInformation($"C# ServiceBus queue trigger function processing message: {myQueueItem}");

                Login login = JsonConvert.DeserializeObject<Login>(myQueueItem);


                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = "rmsdbdev.database.windows.net";
                builder.UserID = "RmsAdmin";
                builder.Password = "P@ssw0rd";
                builder.InitialCatalog = "Rms40CgiDemo";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    Console.WriteLine("\nQuery data example:");
                    Console.WriteLine("=========================================\n");

                    connection.Open();
                    StringBuilder sqlBuilder = new StringBuilder();
                    //sqlBuilder.Append("INSERT INTO sonicTable ");
                    //sqlBuilder.Append(String.Format("VALUES ("+login.timestamp+"," +login.username+"," +login.lastLogin+","+login.email+"," +login.device +")"));


                    sqlBuilder.Append("INSERT INTO sonicTable ");
                    sqlBuilder.Append("VALUES ( @timeStamp , @username , @lastLogin, @email, @device)");
                    using (SqlCommand cmd = new SqlCommand(sqlBuilder.ToString(), connection))
                    {
                        cmd.Parameters.AddWithValue("@timeStamp", login.timestamp);
                        cmd.Parameters.AddWithValue("@username", login.username);
                        cmd.Parameters.AddWithValue("@lastLogin", login.lastLogin);
                        cmd.Parameters.AddWithValue("@email", login.email);
                        cmd.Parameters.AddWithValue("@device", login.device);
                         cmd.ExecuteNonQuery();
                        
                    }
                }
            
            }
            catch(JsonSerializationException e)
            {
                Console.WriteLine(e.ToString());
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.WriteLine("\nDone. Press enter.");

        }
    }
}
