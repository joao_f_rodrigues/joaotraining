﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;

namespace ServiceBus
{
    class ServiceBus
    {
        const string ServiceBusConnectionString = "Endpoint=sb://rmsdev.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=bwLNVj1o3Ex8vbd0QtiNbrPcT5JuwWeYYu40vAEDdAM=";
        const string QueueName = "sonicqueue";
        static IQueueClient queueClient;
        private static Random random = new Random();
        static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
        }

        static async Task MainAsync()
        {
            const int numberOfMessages = 10;
            queueClient = new QueueClient(ServiceBusConnectionString, QueueName);

            // Send messages.
            await SendMessagesAsync(numberOfMessages);

            Console.ReadKey();

            await queueClient.CloseAsync();
        }
        
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static long LongRandom(long min, long max, Random rand)
        {
            byte[] buf = new byte[8];
            rand.NextBytes(buf);
            long longRand = BitConverter.ToInt64(buf, 0);
            return (Math.Abs(longRand % (max - min)) + min);
        }
        static async Task SendMessagesAsync(int numberOfMessagesToSend)
        {
       
            try
            {
                while (true) { 
                    // Create a new message to send to the queue.
                    Login login = new Login();
                    login.timestamp = DateTime.Now.Ticks;
                    login.username = RandomString(random.Next(1, 9));
                    login.email = RandomString(random.Next(1, 9));
                    login.device = RandomString(random.Next(1, 9));
                    login.lastLogin = LongRandom(100000000000000000, 600000000000000000, random);
                    string JSONresult = JsonConvert.SerializeObject(login);

                    var message = new Message(Encoding.UTF8.GetBytes(JSONresult));

                    // Write the body of the message to the console.
                    Console.WriteLine($"Sending message: {JSONresult}");

                    // Send the message to the queue.
                    await queueClient.SendAsync(message);
                    Thread.Sleep(1000);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine($"{DateTime.Now} :: Exception: {exception.Message}");
            }
        }
    }
}
